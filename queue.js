let collection = [];

// Write the queue functions below.


// 1. Output all the elements of the queue
// function print() {
//     return collection;
// }
const print = () => collection;



// 2. Adds element to the rear of the queue
function enqueue(element) {

    let lastIndex = collection.length;

    collection[lastIndex] = element;

    return collection;

}



// 3. Removes element from the front of the queue
function dequeue() {

    // ['red','blue','green]
    // ['blue', 'blue','green'] - 1st
    // ['blue','green','green'] - 2nd

    for (i = 0; i < collection.length - 1; i++) {
        collection[i] = collection[i + 1];
    }

    // Remove last item
    collection.length --
    
    return collection;
}


// 4. Show element at the front
// function front() {
//     return collection[0];
// }
const front = () => collection[0];



// 5. Show the total number of elements
// function size() {
//     return collection.length;
// }
const size = () => collection.length;



// 6. Outputs a Boolean value describing whether queue is empty or not
// function isEmpty() {
//     return collection.length === 0 ? true : false;
// }
const isEmpty = () => collection.length === 0 ? true : false;


module.exports = {
    collection,
    print,
    enqueue,
    dequeue,
    front,
    size,
    isEmpty
};